/*Copyright (C) 2021  Martin Pavelka

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "engine/engine.h"
#include "game_libs/game.h"
/*
 * TODO: add checks to bitmap so that it wont crash on negative numbers...
 * TODO: finish implementing <ragdoll_object_remove_point>
 * TODO: add drawing for unfilled circle
 * NOTE: look at stdarg.h
 * NOTE: finishing ragdoll is HIGH priority now
 * NOTE: look at making shaders with OpenGL, maybe it would be 
 *       better to draw the circles and stuff with shaders
 *       instead of using the cpu to do it
 */  

int main(){
    printf("Copyright (C) 2021  Martin Pavelka\n");
    printf("This program comes with ABSOLUTELY NO WARRANTY\n");

    const int WINDOW_W = 1700;
    const int WINDOW_H = 1000;

    engine_t engine = engine_init(WINDOW_W, WINDOW_H, 0);
    game_t game = game_init(engine); 
    

    while (game.run){
        RETURN_t game_status = game_loop(&game);
        if (game_status != OK) return game_status;
    }
    
    clear_game(&game);
    engine_quit(engine);
    return OK;
}
