/*Copyright (C) 2021  Martin Pavelka

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "engine.h"

void sdl_init(){
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
        error_exit(INIT_ERROR, "SDL init 1", SDL_GetError());
    }
    
    // init SDL_image
    // load support for the JPG and PNG image formats
    int32_t flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int32_t initted=IMG_Init(flags);
    if((initted&flags) != flags) {
        fprintf(stderr,
                "IMG_Init: Failed to init required jpg and png support!\n");
        fprintf(stderr,
                "IMG_Init: %s\n", IMG_GetError());
        error_exit(INIT_ERROR, NULL, NULL);
    }
    
    // text  
    if (TTF_Init() < 0) error_exit(INIT_ERROR,"SDL init 2", 
            "Text init failed");
}


engine_t engine_init(const uint32_t window_W, const uint32_t window_H,
        Uint32 w_flags){
    engine_t engine;
    engine.window_W = window_W;
    engine.window_H = window_H;
    
    sdl_init();
   
    // window 
    SDL_Window* window = SDL_CreateWindow("Game", 0, 0, 
            window_W, window_H, w_flags);
    if (!window) error_exit(INIT_ERROR, "Engine init 1",  SDL_GetError());
    
    engine.window = window;
    
    // renderer
    Uint32 render_flags = SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC;     
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, render_flags);
    if (!renderer) error_exit(INIT_ERROR, "Engine init 2",  SDL_GetError());
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
    engine.renderer = renderer;
    
    // physics
    physics_vars_t physics = {
        .delta_time = 0,
        .gravity = 0.1f,
        .friction = 0.999f,
        .bounce = 0.8f,
        .rigidity = 3,

        .frame_timestamp = SDL_GetTicks(),
        .window_w = window_W,
        .window_h = window_H
    };
    engine.physics = physics;
    return engine;
}


void engine_quit(engine_t engine){
    // close text plugin
    TTF_Quit();

    SDL_DestroyRenderer(engine.renderer);
    SDL_DestroyWindow(engine.window);
    SDL_Quit();
}


void physics_update(physics_vars_t* physics){
    Uint32 time_stamp = SDL_GetTicks();
    physics->delta_time = (time_stamp - physics->frame_timestamp) / 1000.0f; 
    physics->frame_timestamp = time_stamp;
}


void update(engine_t* engine){
    physics_update(&engine->physics);
    SDL_RenderPresent(engine->renderer);
}


void background_color(SDL_Renderer* renderer, color_t color){
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a); 
    SDL_RenderClear(renderer);
}

