#include "color.h"

color_t TRANSPARENT = {0, 0, 0, 0};
color_t BLACK = {0, 0, 0, 255};
color_t WHITE = {255, 255, 255, 255};
color_t RED = {255, 0, 0, 255};
color_t GREEN = {0, 255, 0, 255};
color_t BLUE = {0, 0, 255, 255};
color_t PINK = {255, 0, 127, 255};
color_t YELLOW = {255, 255, 0, 255};
color_t GRAY = {96, 96, 96, 255};
color_t ORANGE = {255, 128, 0, 255};


color_t new_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a){
    color_t color = {
        clamp(0, 255, r),
        clamp(0, 255, g),
        clamp(0, 255, b),
        clamp(0, 255, a)
    };
    return color;
}

