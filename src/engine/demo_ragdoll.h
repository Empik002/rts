#ifndef DEMO_RAGDOLL_H
#define DEMO_RAGDOLL_H

/*
 * These are demo function that will fill object 
 * (via pointer) with sticks and points
 *
 */

#include "ragdoll.h"

void demo_cloth(ragdoll_object_t* o);
void demo_square(ragdoll_object_t* o);
void demo_pendulum(ragdoll_object_t* o);
void demo_stickman(ragdoll_object_t* o);
#endif
