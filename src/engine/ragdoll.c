#include "ragdoll.h"
/* TODO: 
 *       EDIT: I think nothing is wrong with delta time calculation
 *       but the way ragdoll calculates its speed is bad (eg the points 
 *       move too slow) need to check it out
 *              
 *       --something is wrong with calculating delta_time
 *       or with applying it to ragdoll physics
 *       as they speed up with more perfromance---
 *       figured it out, i am not applying it at all
 *       I need to check calculation of delta time
 *       something is wrong (try delta_time * delta_time)
 * TODO: optimise ragdoll somehow
 *       maybe make points store the sticks they are connected to as well
 *       so that it is simpler to remove them
 *
 *       EDIT: here we begin having a problem though
 *       it is hard to actually store them, like this because
 *       points then have to hold another list and removing from it would
 *       be pain and it would become very convoluted. So probably the only
 *       way of actually deleting them is looping over the list of sticks
 *       and removing all the sticks that have a pointer to that point
 *       which can be a lot. 
 */ 
ragdoll_point_t* new_ragdoll_point(int32_t x, int32_t y, 
        int32_t oldx, int32_t oldy, bool locked){
    
    ragdoll_point_t point = {x, y, oldx, oldy, locked, NULL, NULL};
    ragdoll_point_t* pointer = malloc(sizeof(ragdoll_point_t));
    *(pointer) = point;

    return pointer;
}


ragdoll_stick_t* new_ragdoll_stick(ragdoll_point_t* pointA, 
        ragdoll_point_t* pointB, int32_t length){

    ragdoll_stick_t stick = {pointA, pointB, length, NULL, NULL};
    ragdoll_stick_t* pointer = malloc(sizeof(ragdoll_stick_t));
    *(pointer) = stick;
    return pointer;
}


ragdoll_object_t new_ragdoll_object(int32_t x, int32_t y){
    ragdoll_object_t o = {x, y, NULL, NULL, NULL, NULL};
    return o;
}


void ragdoll_object_push_point(ragdoll_object_t* object, 
        ragdoll_point_t* point){
    if (object->points_start == NULL){
        object->points_start = point;
        object->points_end = point;
    } else{
        ragdoll_point_t* last = object->points_end;
        point->prev = last;
        last->next = point;
        object->points_end = point;
    }
}


void ragdoll_object_push_stick(ragdoll_object_t* object, 
        ragdoll_stick_t* stick){
    if (object->sticks_start == NULL){
        object->sticks_start = stick;
        object->sticks_end = stick;
    } else{
        ragdoll_stick_t* last = object->sticks_end;
        stick->prev = last;
        last->next = stick;
        object->sticks_end = stick;
    }
}

// TODO:  this must also remove its sticks !!!!!
void ragdoll_object_remove_point(ragdoll_object_t* object, 
        ragdoll_point_t* point){
    
    printf("%s", "Cant use remove point just yet!!!!!\n");
    error_exit(UNKNOWN, "ragdoll_object_remove_point", "not finished");
    ragdoll_point_t* prev = point->prev;
    ragdoll_point_t* next = point->next;
    if (prev == NULL) object->points_start = point->next;
    else prev->next = next;
    
    if (next == NULL) object->points_end = point->prev;
    else next->prev = prev;

    free(point); 
}


void ragdoll_object_remove_stick(ragdoll_object_t* object,
        ragdoll_stick_t* stick){
    ragdoll_stick_t* prev = stick->prev;
    ragdoll_stick_t* next = stick->next;
    if (prev == NULL) object->sticks_start = stick->next;
    else prev->next = next;
    
    if (next == NULL) object->sticks_end = stick->prev;
    else next->prev = prev;

    free(stick); 
}


void constrain_ragdoll_points(physics_vars_t* physics,
        ragdoll_object_t* object){
    uint32_t width = physics->window_w;
    uint32_t height = physics->window_h;
    ragdoll_point_t* point = object->points_start;
    while (point != NULL){
        if (!point->locked){
            float vx = (point->x - point->oldx) * physics->friction;
            float vy = (point->y - point->oldy) * physics->friction;  

            if (point->x > width){
                point->x = width;
                point->oldx = point->x + vx * physics->bounce; 
            }else if(point->x < 0){
                point->x = 0;
                point->oldx = point->x + vx * physics->bounce;
            }
            if (point->y > height){
                point->y = height;
                point->oldy = point->y + vy * physics->bounce;
            }else if(point->y < 0){
                point->y = 0;
                point->oldy = point->y + vy * physics->bounce;
            }
        }
        point = point->next;
    }
}


void update_ragdoll_points(physics_vars_t* physics,
        ragdoll_object_t* object){
    ragdoll_point_t* point = object->points_start;
    while (point != NULL){
        if (!point->locked){
            float vx = (point->x - point->oldx) * physics->friction;
            float vy = (point->y - point->oldy) * physics->friction;  
            point->oldx = point->x;
            point->oldy = point->y;

            point->x += vx ;
            point->y += vy;
            point->y += physics->gravity;
        }
        point = point->next;
    }
}


void update_ragdoll_sticks(ragdoll_object_t* object){
    ragdoll_stick_t* stick = object->sticks_start;
    while(stick != NULL){
        ragdoll_point_t* p1 = stick->pointA;
        ragdoll_point_t* p2 = stick->pointB; 
    
        float dist = ragdoll_distance(p1, p2);
        float dx = p2->x - p1->x;
        float dy = p2->y - p1->y;
        float diff = stick->length - dist;
        float percent = diff / dist / 2;

        // calculate how much to move points on x and y axes
        float offset_x = dx * percent;
        float offset_y = dy * percent;
        
        if (p1->locked){
            p2->x += 2 * offset_x;
            p2->y += 2 * offset_y;
        } else if (p2->locked){
            p1->x -= 2 * offset_x;
            p1->y -= 2 * offset_y;
        }else{
            p1->x -= offset_x;
            p1->y -= offset_y;
            p2->x += offset_x;
            p2->y += offset_y;
        }
        stick = stick->next;
    }
}


float ragdoll_distance(ragdoll_point_t* p1, ragdoll_point_t* p2){
    return distance(p1->x, p1->y, p2->x, p2->y);
}


void render_ragdoll(SDL_Renderer* renderer, ragdoll_object_t* object){
    //ragdoll_point_t* p = object->points_start;
    /*
     *while (p!=NULL){
     *    circle(renderer, 
     *            point_coord(object, p, true), 
     *            point_coord(object, p, false), 
     *            5, p->locked ? RED : WHITE); 
     *    p = p->next;
     *}
     */
    ragdoll_stick_t* s = object->sticks_start;
    while (s!=NULL){
        line(renderer, 
                point_coord(object, s->pointA, true), 
                point_coord(object, s->pointA, false), 
                point_coord(object, s->pointB, true), 
                point_coord(object, s->pointB, false)
                , WHITE);  
        s = s->next;
    }

}


void run_ragdoll(physics_vars_t* physics, ragdoll_object_t* object){
    update_ragdoll_points(physics, object); 
    for(int32_t i = 0; i < max(physics->rigidity, 0); i++){
        constrain_ragdoll_points(physics, object);
        update_ragdoll_sticks(object);
    }
}


void destroy_ragdoll_object(ragdoll_object_t* object){
    ragdoll_point_t* point = object->points_start;
    while (point!=NULL){
        ragdoll_point_t* next = point->next;
        free(point);
        point = next;
    }
    ragdoll_stick_t* stick = object->sticks_start;
    while (stick!=NULL){
        ragdoll_stick_t* next = stick->next;
        free(stick);
        stick=next;
    }

    object->points_start = NULL;
    object->points_end = NULL;
    object->sticks_start = NULL;
    object->sticks_end = NULL;
}


int point_coord(ragdoll_object_t* o, ragdoll_point_t* p, bool x){
    if (x) return o->x + p->x;
    return o->y + p->y;
}


