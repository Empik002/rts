#include <stdio.h>
#include <stdbool.h>

#include "draw.h"

void draw_shape(SDL_Renderer* renderer, SDL_Texture* texture,
        int32_t x, int32_t y){
    blit_texture(renderer, texture, x, y);
}


void rect(SDL_Renderer* renderer, 
        int32_t x, int32_t y, int32_t w, int32_t h,
        color_t color,
        bool filled){
    
    SDL_Rect rect;
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;
    
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    if (filled){
        SDL_RenderFillRect(renderer, &rect);
    }else{
        SDL_RenderDrawRect(renderer, &rect);
    }
}


void line(SDL_Renderer* renderer, 
        int32_t x1, int32_t y1, int32_t x2, int32_t y2,
        color_t color){
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
}


void point(SDL_Renderer* renderer,
        int32_t x, int32_t y, color_t color){
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawPoint(renderer, x, y);    
}


void points(SDL_Renderer* renderer, SDL_Point* point_array, uint32_t count,
        color_t color){
    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    SDL_RenderDrawPoints(renderer, point_array, count);
}


void circle(SDL_Renderer* renderer, 
        int32_t midx, int32_t midy, int32_t radius,
        color_t color){
    
    for (int32_t i = midx - radius; i <= midx + radius; i++){
        for (int32_t j = midy - radius; j <= midy + radius; j++){
            if (distance(i, j, midx, midy) <= radius){
                point(renderer, i, j, color);
            }else{
                point(renderer, i, j, TRANSPARENT);
            } 
        }
    }
}


void unfilled_triangle(SDL_Renderer* renderer,
        int32_t x1, int32_t y1,
        int32_t x2, int32_t y2,
        int32_t x3, int32_t y3,
        color_t color){
    SDL_SetRenderDrawColor(renderer, color.r, color.b, color.b, color.a);
    line(renderer, x1, y1, x2, y2, color);
    line(renderer, x1, y1, x3, y3, color);
    line(renderer, x2, y2, x3, y3, color); 
}


void triangle(SDL_Renderer* renderer,
        int32_t x1, int32_t y1,
        int32_t x2, int32_t y2,
        int32_t x3, int32_t y3,
        color_t color, bool filled){
    
    if (!filled){
        unfilled_triangle(renderer, x1, y1, x2, y2, x3, y3, color);
        return;
    }

    
    int32_t min_x = min(min(x1, x2), x3);
    int32_t max_x = max(max(x1, x2), x3);
    int32_t min_y = min(min(y1, y2), y3);
    int32_t max_y = max(max(x1, x2), x3);
    
    point_t A = {x1, y1};
    point_t B = {x2, y2};
    point_t C = {x3, y2};
    
    for (int32_t i = min_x; i <= max_x; i++){
        for (int32_t j = min_y; j <= max_y; j++){
            point_t P = {i, j};
            if (is_in_triangle(A, B, C, P)){
                point(renderer, i, j, color);
            }else{
                point(renderer, i, j, TRANSPARENT);
            }
        }
    }
}


/* texture shapes */
SDL_Texture* rect_texture(SDL_Renderer* renderer, 
        int32_t w, int32_t h, 
        color_t color, bool filled){

    SDL_Texture* texture = editable_texture(renderer, w, h);
    lock_texture(renderer, texture);
    // fill with transparent
    rect(renderer, 0, 0, w, h, TRANSPARENT, true); 
    // fill actual rect 
    rect(renderer, 0, 0, w, h, color, filled);    
    unlock_texture(renderer, texture);
    return texture;
}


SDL_Texture* circle_texture(SDL_Renderer* renderer,
        int32_t radius,
        color_t color){

    SDL_Texture* texture = editable_texture(renderer, radius*2, radius*2);
    lock_texture(renderer, texture);

    circle(renderer, radius, radius, radius, color); 

    unlock_texture(renderer, texture);
    return texture; 
}

