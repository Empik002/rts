#ifndef TEXTURES_H
#define TEXTURES_H

#include "sdl.h"

typedef struct texture_info_t{
    int32_t w;
    int32_t h;
    int32_t access;
    uint32_t format;
} texture_info_t;

typedef struct texture_list_t{
    SDL_Texture** list;
    uint32_t length;
    uint32_t max_length;
} texture_list_t;

// alias for library function IMG_LoadTexture
// creates texture from an image path, on error returns NULL
SDL_Texture* image_texture(SDL_Renderer* renderer, const char* path);

// returns blank textura that can be rendered onto
SDL_Texture* editable_texture(SDL_Renderer* renderer, uint32_t w, uint32_t h);
// returns struct with TextureQuery stuff
texture_info_t get_texture_info(SDL_Texture* texture);

// setups texture for drawing (changes renderer draw mode as well)
void lock_texture(SDL_Renderer* renderer, SDL_Texture* texture);
// reverts locked texture
void unlock_texture(SDL_Renderer* renderer, SDL_Texture* texture);

// blits a texture at given coordinates
void blit_texture(SDL_Renderer* renderer, SDL_Texture* texture,
        int32_t x, int32_t y);

#endif
