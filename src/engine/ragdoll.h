#ifndef RAGDOLL_H
#define RAGDOLL_H

#include <stdbool.h>
#include "draw.h"

/*
 * Ragdoll points keep their posision as an offset of the objects position
 * So when rendering you have to do object.x/y + point.x/y
 * same with sticks
 * or you can use the <<<<point_coord>>>> function 
 */ 

typedef struct ragdoll_point_t{
    float x;
    float y;
    float oldx;
    float oldy;
    bool locked;
    struct ragdoll_point_t* next; 
    struct ragdoll_point_t* prev;
} ragdoll_point_t;

typedef struct ragdoll_stick_t{
    ragdoll_point_t* pointA;
    ragdoll_point_t* pointB;
    float length;
    struct ragdoll_stick_t* next;
    struct ragdoll_stick_t* prev;
} ragdoll_stick_t;
 
typedef struct ragdoll_object_t{
    int32_t x;
    int32_t y;
    ragdoll_point_t* points_start;
    ragdoll_point_t* points_end;
    ragdoll_stick_t* sticks_start;
    ragdoll_stick_t* sticks_end;
} ragdoll_object_t;

typedef struct physics_vars_t{
    float delta_time;
    float gravity;
    float friction;
    float bounce; 
    int32_t rigidity; // number of iterations for constraints (min 1, default 3)
                  // higher = slower    
    uint32_t frame_timestamp; // time of last frame
    uint32_t window_w;
    uint32_t window_h;
} physics_vars_t;

ragdoll_point_t* new_ragdoll_point(int32_t x, int32_t y, 
        int32_t oldx, int32_t oldy, bool locked);

ragdoll_stick_t* new_ragdoll_stick(ragdoll_point_t* pointA, 
        ragdoll_point_t* pointB, int32_t length);


/* returns ragdoll_object_t with mallocd list for sticks and points
 * if mallocing fails, returns ragdoll_object_t with NULL for both lists
 * and both lengths set to -1 */
ragdoll_object_t new_ragdoll_object(int32_t x, int32_t y);


void ragdoll_object_push_point(ragdoll_object_t* object, 
        ragdoll_point_t* point);
void ragdoll_object_push_stick(ragdoll_object_t* object,
        ragdoll_stick_t* stick);
void ragdoll_object_remove_point(ragdoll_object_t* object, 
        ragdoll_point_t* point);
void ragdoll_object_remove_stick(ragdoll_object_t* object,
        ragdoll_stick_t* stick);

void constrain_ragdoll_points(physics_vars_t* physics, 
        ragdoll_object_t* object);

void update_ragdoll_points(physics_vars_t* physics, ragdoll_object_t* object);

void update_ragdoll_sticks(ragdoll_object_t* object);

float ragdoll_distance(ragdoll_point_t* p1, ragdoll_point_t* p2);

/* optional drawing function */
void render_ragdoll(SDL_Renderer* renderer, ragdoll_object_t* object);
void run_ragdoll(physics_vars_t* physics, ragdoll_object_t* object);

void destroy_ragdoll_object(ragdoll_object_t* object);

// the x parameter specifies if you want x coord (true) or y (false)
int point_coord(ragdoll_object_t* o, ragdoll_point_t* p, bool x);
#endif
