#ifndef BITMAP_H
#define BITMAP_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

/* This mocdule creates a bitmap (full of zeros), standard is a 1D bitmap
 * and the other functions (like bitmap2D) build only an interface
 * on the 1D bitmap, the bitmap contains a malloced list
 *
 *
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * get bit functions return 2 on error (a.k.a. index out of bounds)
 */


typedef struct bitmap_t{
    //front padding before actuall bitmap starts
    // number of bits
    uint8_t padding;
    uint64_t map_len; // number of bytes in map
    uint8_t* map; // malloced list
} bitmap_t;

bitmap_t new_bitmap(uint64_t num_of_bits);

// returns 2 on error
uint8_t get_bit(bitmap_t map, int64_t index);
uint8_t get_bit_from_byte(uint8_t byte, uint8_t index);

// returns false on error (aka index out of bounds)
// tru on sucess
bool set_bit(bitmap_t map, int64_t index, uint8_t bit);
// returns the same byte if new_bit is neither 0 nor 1
uint8_t set_bit_in_byte(uint8_t byte, uint8_t index, uint8_t new_bit);

bitmap_t new_bitmap2D(uint64_t width, uint64_t height);
uint8_t get_bit2D(bitmap_t map, uint64_t x, uint64_t y);

#endif
