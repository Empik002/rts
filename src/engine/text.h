#ifndef FONT_H
#define FONT_H

#include <stdio.h>
#include <stdbool.h>

#include "sdl.h"
#include "color.h"
#include "util.h"

TTF_Font* open_font(const char* path, int32_t size);

// quite costly to create, should not use for changing text
// simplest possible way to render text
SDL_Texture* text_texture(SDL_Renderer* renderer, 
        const char* text, TTF_Font* font, color_t color);

#endif
