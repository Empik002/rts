#ifndef COLOR_H
#define COLOR_H

#include <stdint.h>
#include "util.h"

typedef struct color_t{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
} color_t;


extern color_t TRANSPARENT;
extern color_t WHITE; 
extern color_t BLACK;
extern color_t RED;
extern color_t GREEN;
extern color_t BLUE;
extern color_t PINK;
extern color_t YELLOW;
extern color_t GRAY;
extern color_t ORANGE;

color_t new_color(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
#endif
