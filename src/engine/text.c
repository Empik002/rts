#include "text.h"

// TODO: Create more effective rendereing method, or use
// extension SDL_FontCache

TTF_Font* open_font(const char* path, int32_t size){
    TTF_Font* font = TTF_OpenFont(path, size);
    if (!font) error_exit(WRONG_INPUT, "Open font 1", TTF_GetError());
    return font;
}


SDL_Texture* text_texture(SDL_Renderer* renderer, 
        const char* text, TTF_Font* font, color_t color){
    
    if (!font) error_exit(WRONG_INPUT, "Text texture 1", "No font specified!");

    SDL_Surface* text_surface;
    SDL_Color sdl_color = {color.r, color.g, color.b, color.a};
    text_surface = TTF_RenderText_Solid(font, text, sdl_color);
    if (!text) error_exit(UNKNOWN, "Text texture 2", TTF_GetError());

    SDL_Texture* text_texture;
    text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);

    SDL_FreeSurface(text_surface);
    return text_texture;
}
