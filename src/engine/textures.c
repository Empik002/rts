#include <stdio.h>
#include <stdbool.h>

#include "textures.h"

#define UNUSED(x) (void)(x)

SDL_Texture* image_texture(SDL_Renderer* renderer, const char* path){
    return IMG_LoadTexture(renderer, path);
}
    

SDL_Texture* editable_texture(SDL_Renderer* renderer, uint32_t w, uint32_t h){
    return SDL_CreateTexture(renderer, 
            SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_TARGET, w, h); 
}


texture_info_t get_texture_info(SDL_Texture* texture){
    texture_info_t info;
    SDL_QueryTexture(texture, &info.format, &info.access, &info.w, &info.h);
    return info;
}


void lock_texture(SDL_Renderer* renderer, SDL_Texture* texture){
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_NONE);
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
    SDL_SetRenderTarget(renderer, texture);
}


void unlock_texture(SDL_Renderer* renderer, SDL_Texture* texture){
    // used for consistency and to make sure you dont unlock texture
    // you dont know about
    UNUSED(texture);
    
    SDL_SetRenderTarget(renderer, NULL);
    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);
}


void blit_texture(SDL_Renderer* renderer, SDL_Texture* texture,
        int32_t x, int32_t y){
    texture_info_t info = get_texture_info(texture); 
    SDL_Rect dest = {
        .x = x,
        .y = y,
        .w = info.w,
        .h = info.h
    };
    SDL_RenderCopy(renderer, texture, NULL, &dest); 
}


