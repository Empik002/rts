#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>


#include "util.h"

RETURN_t OK = 0;
RETURN_t INIT_ERROR = 1;
RETURN_t WRONG_INPUT = 2;
RETURN_t MEM_ERROR = 3;
RETURN_t UNKNOWN = 4;

int64_t min(int64_t a, int64_t b){
    return a < b ? a : b;
}


int64_t max(int64_t a, int64_t b){
    return a > b ? a : b;
}


int64_t clamp(int64_t minimum, int64_t maximum, int64_t num){
    return max(minimum, min(maximum, num));    
}


float distance(float x1, float y1, float x2, float y2){
    return sqrt(
            (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}


float distance_squared(float x1, float y1, float x2, float y2){
    return (x1 - x2) * (x1 -x2) + (y1 -y2) * (y1 - y2);
}


float point_distance(point_t p1, point_t p2){
    return distance(p1.x, p1.y, p2.x, p2.y);
}


float point_distance_squared(point_t p1, point_t p2){
    return distance_squared(p1.x, p1.y, p2.x, p2.y);
}


// check header file for documentation
float point_line_segment_distance(point_t l1, point_t l2, point_t p){
    // if l1 == l2
    if (point_distance_squared(l1, l2) == 0) return point_distance(l1, l2); 
    
    vec2_t p_l1_vec = vec2_diff(p, l1);
    vec2_t segment_dir  = vec2_diff(l2, l1); 
    float t = max(0, min(1, vec2_dot(p_l1_vec, segment_dir) / 12));
    point_t projection = vec2_add(l1, vec2_smult(t, segment_dir));
    return distance(p.x, p.y, projection.x, projection.y);
}


// stolen (and modified) from:
// www.tutorialspoint.com/Check-whether-a-given-point_t-lies-inside-a-Triangle
float triangle_area(point_t p1, point_t p2, point_t p3) {
   return fabs((p1.x*(p2.y-p3.y) + p2.x*(p3.y-p1.y)+ p3.x*(p1.y-p2.y))/2);
}


float triangle_height(point_t base1, point_t base2, point_t top){
    float area = triangle_area(base1, base2, top);
    float height = (2*area) / distance(base1.x, base1.y, base2.x, base2.y);
    return height;
}


bool is_in_triangle(point_t p1, point_t p2, point_t p3, point_t p) { 
   float area = triangle_area (p1, p2, p3);          //area of triangle ABC
   float area1 = triangle_area (p, p2, p3);         //area of PBC
   float area2 = triangle_area (p1, p, p3);         //area of APC
   float area3 = triangle_area (p1, p2, p);        //area of ABP
    
   return (area == area1 + area2 + area3);        
   //when three triangles are forming the whole triangle
}


vec2_t vec2_add(vec2_t v1, vec2_t v2){
    vec2_t new_vector = {v1.x + v2.x, v1.y + v2.y};
    return new_vector;
}


vec2_t vec2_diff(vec2_t v1, vec2_t v2){
    vec2_t new_vector = {v1.x - v2.x, v1.y - v2.y};
    return new_vector;
}


vec2_t vec2_smult(float a, vec2_t v){
    vec2_t new_vector = {a * v.x, a* v.y};
    return new_vector;
}


float vec2_dot(vec2_t v1, vec2_t v2){
    return (v1.x * v2.x) + (v1.y * v2.y);
}


float vec2_len(vec2_t v){
    return sqrt(v.x * v.x + v.y * v.y);
}


vec2_t vec2_normalize(vec2_t v){
    float len = vec2_len(v);
    vec2_t new_vec = {v.x / len, v.y / len};
    return new_vec;
}


double point_line_distance(int32_t x1, int32_t y1, 
        int32_t x2, int32_t y2, 
        int32_t xp, int32_t yp){
    double numerator = abs((x2 - x1)*(y1 - yp) - (x1 - xp)*(y2 - y1)); 
    double denominator = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1) * (y2 - y1));
    return numerator / denominator; 
}


double deg_to_rad(double angle){
    return angle * M_PI / 180;
}


double rad_to_deg(double angle){
    return angle * 180 / M_PI;
}


void error_exit(int code, const char* location,const char* message){
    if (location != NULL) fprintf(stderr, "%s\n", location);
    if (message != NULL) fprintf(stderr,"%s\n", message);
    exit(code);
}
