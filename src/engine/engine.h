#ifndef ENGINE_H
#define ENGINE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "sdl.h"
#include "util.h"
#include "textures.h"
#include "draw.h"
#include "color.h"
#include "ragdoll.h"
#include "text.h"

typedef struct engine_t{
    SDL_Window* window;
    SDL_Renderer* renderer;

    int32_t window_W;
    int32_t window_H;

    physics_vars_t physics;
} engine_t;

void sld_init();

/*
 * initialises opengl and window, w_flags are SDL_window_flags, default 0
 */
engine_t engine_init(const uint32_t window_W, const uint32_t window_H,
        Uint32 w_flags);

/*
 * Turns off SDL and closes window
 *
 */
void engine_quit(engine_t engine);

void physics_update(physics_vars_t* physics);

/*
 * run physics update 
 * Switches frames
 */
void update(engine_t* engine);

void background_color(SDL_Renderer* renderer, color_t color);

#endif
