#include "demo_ragdoll.h"
#include <time.h>

#define COLS 80
#define ROWS 80

void demo_cloth(ragdoll_object_t* o){
    // init random seed for making the cloth sway
    srand((unsigned int) time(NULL));
    
    const uint32_t w = 1700;
    const uint32_t h = 700;

    ragdoll_point_t* row[COLS] = {NULL};
    ragdoll_point_t* row_above[COLS] = {NULL};
    
    // cloth is grid so we can precalculate distance and use it everywhere
    float h_dist = (float) w / COLS;
    float v_dist = (float) h / ROWS;
    for (int32_t r = 0; r<ROWS; r++){ 
        // create row of points
        for (int32_t c = 0; c<COLS; c++){   
            int32_t x = (w/COLS)*c;
            int32_t y = (h/ROWS)*r; // first row is locked vvv
            row[c] = new_ragdoll_point(x, y, x+rand()%5, y, r == 0);
            ragdoll_object_push_point(o, row[c]);
        }
        
        // connect the points
        for (int32_t i = 0; i<COLS-1; i++){
            // connect horizontaly
            ragdoll_object_push_stick(o, 
                    new_ragdoll_stick(row[i], row[i+1], h_dist)); 
        }

        // conect vertically (to the dot above)
        for (int32_t i = 0; i<COLS; i++){
            if (r == 0) continue; // skip first row
            ragdoll_object_push_stick(o, 
                    new_ragdoll_stick(row[i], row_above[i], v_dist));
        }

        //copy row to row_above
        for (int32_t i = 0; i<COLS; i++){
            row_above[i] = row[i];
        }
    }
}


void demo_square(ragdoll_object_t* o){
    ragdoll_point_t* p1 = new_ragdoll_point(15, 15, -5, 15, false);
    ragdoll_point_t* p2 = new_ragdoll_point(65, 15, 65, 15, false);
    ragdoll_point_t* p3 = new_ragdoll_point(15, 65, 15, 65, false);
    ragdoll_point_t* p4 = new_ragdoll_point(65, 65, 65, 65, false);
    
    ragdoll_object_push_point(o, p1);
    ragdoll_object_push_point(o, p2);
    ragdoll_object_push_point(o, p3);
    ragdoll_object_push_point(o, p4);

    ragdoll_stick_t* s1 = new_ragdoll_stick(p1, p2, 50); 
    ragdoll_stick_t* s2 = new_ragdoll_stick(p1, p3, 50);
    ragdoll_stick_t* s3 = new_ragdoll_stick(p2, p4, 50);
    ragdoll_stick_t* s4 = new_ragdoll_stick(p3, p4, 50);
    ragdoll_stick_t* s5 = new_ragdoll_stick(p1, p4, 
            ragdoll_distance(p1, p4));

    ragdoll_object_push_stick(o, s1);
    ragdoll_object_push_stick(o, s2);
    ragdoll_object_push_stick(o, s3);
    ragdoll_object_push_stick(o, s4);
    ragdoll_object_push_stick(o, s5);
}


void demo_pendulum(ragdoll_object_t* o){
    ragdoll_point_t* p1 = new_ragdoll_point(250, 200, 250, 200, false);
    ragdoll_point_t* p2 = new_ragdoll_point(200, 250, 200, 250, false);
    ragdoll_point_t* p3 = new_ragdoll_point(300, 250, 300, 250, false);
    ragdoll_point_t* p4 = new_ragdoll_point(250, 300, 250, 300, false);
    ragdoll_point_t* p5 = new_ragdoll_point(200, 150, 200, 150, false);
    ragdoll_point_t* p6 = new_ragdoll_point(170, 100, 170, 100, true);


    ragdoll_object_push_point(o, p1);
    ragdoll_object_push_point(o, p2);
    ragdoll_object_push_point(o, p3);
    ragdoll_object_push_point(o, p4);
    ragdoll_object_push_point(o, p5);
    ragdoll_object_push_point(o, p6);
    
    ragdoll_stick_t* s1 = new_ragdoll_stick(p1, p2, ragdoll_distance(p1, p2));
    ragdoll_stick_t* s2 = new_ragdoll_stick(p2, p4, ragdoll_distance(p2, p4));
    ragdoll_stick_t* s3 = new_ragdoll_stick(p4, p3, ragdoll_distance(p4, p3));
    ragdoll_stick_t* s4 = new_ragdoll_stick(p3, p1, ragdoll_distance(p3, p1));
    ragdoll_stick_t* s5 = new_ragdoll_stick(p1, p5, ragdoll_distance(p1, p5));
    ragdoll_stick_t* s6 = new_ragdoll_stick(p5, p6, ragdoll_distance(p5, p6));
    ragdoll_stick_t* s7 = new_ragdoll_stick(p1, p4, ragdoll_distance(p1, p4));
    ragdoll_stick_t* s8 = new_ragdoll_stick(p2, p3, ragdoll_distance(p2, p3));

    ragdoll_object_push_stick(o, s1);
    ragdoll_object_push_stick(o, s2);
    ragdoll_object_push_stick(o, s3);
    ragdoll_object_push_stick(o, s4);
    ragdoll_object_push_stick(o, s5);
    ragdoll_object_push_stick(o, s6);
    ragdoll_object_push_stick(o, s7);
    ragdoll_object_push_stick(o, s8);
}


void demo_stickman(ragdoll_object_t* o){
    ragdoll_point_t* p1 = new_ragdoll_point(100, 100, 250, 250, true); 
    ragdoll_object_push_point(o, p1);
}
