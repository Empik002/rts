#ifndef DRAW_H
#define DRAW_H

#include "sdl.h"
#include "util.h"
#include "textures.h"
#include "color.h"

// alias for blitTexture 
void draw_shape(SDL_Renderer* renderer, SDL_Texture* texture,
        int32_t x, int32_t y);


/*
 * SECTION:
 * Shapes rendered directly to the screen
 *
 */
void rect(SDL_Renderer* renderer, 
        int32_t x, int32_t y, int32_t w, int32_t h, 
        color_t color,
        bool filled); 

void line(SDL_Renderer* renderer, 
        int32_t x1, int32_t y1, int32_t x2, int32_t y2,
        color_t color);

void point(SDL_Renderer* renderer,
        int32_t x, int32_t y, color_t color);

void points(SDL_Renderer* renderer, SDL_Point* point_array, uint32_t count,
        color_t color);

void circle(SDL_Renderer* renderer, 
        int32_t midx, int32_t midy, int32_t radius,
        color_t color);

void unfilled_triangle(SDL_Renderer* renderer,
        int32_t x1, int32_t y1,
        int32_t x2, int32_t y2,
        int32_t x3, int32_t y3,
        color_t color);

void triangle(SDL_Renderer* renderer,
        int32_t x1, int32_t y1,
        int32_t x2, int32_t y2,
        int32_t x3, int32_t y3,
        color_t color, bool filled);


/*
 * SECTION:
 * Shapes rendered to a texture
 *
 */

// creates filled rectangle (on texture)
SDL_Texture* rect_texture(SDL_Renderer* renderer, 
        int32_t w, int32_t h, 
        color_t color, bool filled);

SDL_Texture* circle_texture(SDL_Renderer* renderer,
        int32_t radius,
        color_t color);
 
#endif
