#include "bitmap.h"

uint8_t get_bit_from_byte(uint8_t byte, uint8_t index){
    // calculate index form right
    uint8_t r_index = 7 - index;

    return (byte & (1 << r_index)) >> r_index;
}

uint8_t set_bit_in_byte(uint8_t byte, uint8_t index, uint8_t new_bit){
    // calculate index form right
    uint8_t r_index = 7 - index;

    if (new_bit == 0){
        return byte & ~(1 << r_index);  
    }else if (new_bit == 1){
        return byte | (1 << r_index); 
    }
    
    return byte; 
}

bitmap_t new_bitmap(uint64_t num_of_bits){
    if (num_of_bits <= 0){    
        bitmap_t map = {0, 0, NULL};
        return map;
    }    
    

    uint64_t num_of_bytes = (num_of_bits / 8) + 1; // integer division
    uint8_t padding = (num_of_bytes * 8) - num_of_bits;
    uint8_t* map_list = calloc(1, num_of_bytes);
    bitmap_t map = {padding, num_of_bytes, map_list};
    return map;
}


uint8_t get_bit(bitmap_t map, int64_t index){
    if (index < 0) return 2;
    int64_t map_bits = map.map_len * 8;
    // acount for padding 
    index += map.padding;
    // check if index is not out of bounds
    if (index > map_bits - 1) return 2;

    // calculate correct byte index
    uint64_t byte_index = index / 8; // integer division
    uint8_t bit_index = index % 8;
    return get_bit_from_byte(map.map[byte_index], bit_index); 
}

// maybe put this stuff in a function
bool set_bit(bitmap_t map, int64_t index, uint8_t bit){
    if (index < 0) return false;
    int64_t map_bits = map.map_len * 8;
    // acount for padding 
    index += map.padding;
    // check if index is not out of bounds
    if (index > map_bits - 1) return false;

    // calculate correct byte index
    uint64_t byte_index = index / 8; // integer division
    uint8_t bit_index = index % 8;
    map.map[byte_index] = set_bit_in_byte(map.map[byte_index], bit_index, bit);
    return true;
}

