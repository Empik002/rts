#ifndef UTIL_H
#define UTIL_H

#include <stdbool.h>
#include <stdint.h>

#ifndef M_PI 
#define M_PI 3.1415926535 
#endif

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif


#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0') 



typedef int RETURN_t;

extern RETURN_t OK;
extern RETURN_t INIT_ERROR;
extern RETURN_t WRONG_INPUT;
extern RETURN_t MEM_ERROR;
extern RETURN_t UNKNOWN;

typedef struct point_t {
    float x;
    float y;
} point_t;
typedef point_t vec2_t;


int64_t min(int64_t a, int64_t b);
int64_t max(int64_t a, int64_t b);
int64_t clamp(int64_t minimum, int64_t maximum, int64_t num);
// distance between two point_ts
float distance(float x1, float y1, float x2, float y2);
float distance_squared(float x1, float y1, float x2, float y2);

float point_distance(point_t p1, point_t p2);
float point_distance_squared(point_t p1, point_t p2);

// stolen from the accepted answer here:
// https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
//  ===find distance of a point from a line segment===
// Consider the line extending the segment, parameterized as v + t (w - v).
// We find projection of point p onto the line. 
// It falls where t = [(p-v) . (w-v)] / |w-v|^2
// We clamp t from [0,1] to handle points outside the segment vw.
float point_line_segment_distance(point_t l1, point_t l2, point_t p);

float triangle_area(point_t p1, point_t p2, point_t p3);
float triangle_height(point_t base1, point_t base2, point_t top);

bool is_in_triangle(point_t p1, point_t p2, point_t p3, point_t p); 

// x1, y1, x2, y2 are the points on the line, xp , yp are the point
// you want to get distance form the line for
double point_line_distance(int32_t x1, int32_t y1, int32_t x2, int32_t y2, 
        int32_t xp, int32_t yp);

vec2_t vec2_add(vec2_t v1, vec2_t v2);
vec2_t vec2_diff(vec2_t v1, vec2_t v2);
float vec2_dot(vec2_t v1, vec2_t v2);

vec2_t vec2_smult(float a, vec2_t v); 
float vec2_len(vec2_t v);

vec2_t vec2_normalize(vec2_t v);
/* Stops execution of program, returns error code and prints to stderr
 * (if message is not null)
 */
void error_exit(RETURN_t code, const char* location, const char* message);
double deg_to_rad(double angle);
double rad_to_deg(double angle);
#endif

