/*Copyright (C) 2021  Martin Pavelka

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include "../engine/demo_ragdoll.h"
#include "game.h"

#define UNUSED(x) (void)(x)

game_t game_init(engine_t engine){
    game_t game = {
        .status = OK,
        .run = true,

        .engine = engine, 
        .window   = engine.window,
        .renderer = engine.renderer,
        .window_W = engine.window_W,
        .window_H = engine.window_H,
        .mouse_x = 0,
        .mouse_y = 0,

        .keyboard_actions= {NULL},
        .left_click_action = &create_point,
        .right_up = true,

        .textures = {NULL},
        .textures_len = 0,

        .ragdoll = new_ragdoll_object(0, 0),
        .input_point_locked = false,
        .run_physics = true,
         
        .font_40 = open_font("img/Fruktur-Regular.ttf", 45)
    };
    game.textures[0] = rect_texture(game.renderer, 
            50, 75, BLACK, false);
    game.textures_len += 1;
    
    game.textures[1] = circle_texture(game.renderer, 100, PINK);
    game.textures_len += 1;    
    
    game.textures[2] = image_texture(game.renderer,
            "/home/empik/Documents/PROJECTS/rts/build/img/unicorn.png");
    game.textures_len += 1;
    
    demo_cloth(&game.ragdoll);

    map_key(&game, 'q', &toggle_locked);
    map_key(&game, 'w', &toggle_simulation);
    return game;
}


void map_key(game_t* game, char key, 
        void (*action)(SDL_Event event, game_t* game)){
    game->keyboard_actions[(uint8_t) key] = action;
}


void run_key(SDL_Event event, game_t* game, char key){
    void (*action)(SDL_Event event, 
            game_t* game) = game->keyboard_actions[(uint8_t) key];
    if (action != NULL) action(event, game);
}


ragdoll_stick_t* get_intersecting_stick(ragdoll_object_t* object,
        int x, int y, int radius){
    ragdoll_stick_t* s = object->sticks_start;
    while (s != NULL){
        ragdoll_point_t* p1 = s->pointA;
        ragdoll_point_t* p2 = s->pointB;

        point_t base1 = {point_coord(object, p1, true), 
            point_coord(object, p1, false)};
        point_t base2 = {point_coord(object, p2, true),
            point_coord(object, p2, false)};
        point_t top = {x, y};

        float dist = point_line_segment_distance(base1, base2, top);
        if (dist <= radius){
            return s;
        } 
        s = s->next;
    }
    return NULL;
}


void create_point(game_t* game){
    ragdoll_point_t* point = new_ragdoll_point(
            game->mouse_x, game->mouse_y, 
            game->mouse_x - 5, game->mouse_y - 5, game->input_point_locked);
    ragdoll_object_push_point(&(game->ragdoll), point); 
}


void delete_stick(game_t* game){
    int RADIUS = 20;
    int MAX_STICKS_PER_FRAME = 100;
    
    ragdoll_stick_t* stick;
    
    for (int i = 0; i < MAX_STICKS_PER_FRAME; i++){
        stick = get_intersecting_stick(
            &game->ragdoll, game->mouse_x, game->mouse_y, RADIUS);
        if (!stick) break;
        ragdoll_object_remove_stick(&game->ragdoll, stick);

    }
}


void toggle_locked(SDL_Event event, game_t* game){
    UNUSED(event);
    game->input_point_locked = !game->input_point_locked;
}


void clear_game(game_t* game){
    // remove textures
    for (int i = 0; i < game->textures_len; i++){
        SDL_DestroyTexture(game->textures[i]); 
    }
    
    // close font
    TTF_CloseFont(game->font_40);
    
    destroy_ragdoll_object(&game->ragdoll);    
}


void toggle_simulation(SDL_Event event, game_t* game){
    UNUSED(event);
    game->run_physics = !game->run_physics;
}


void quit(game_t* game){
    printf("Quitting game\n");
    game->run = false; 
}


// TODO: rework this loop
// idea:
// make this like you would in python (or at least similar)
// take a list of size 255 (the number of ascii characters)
// and populate it with desired function pointers
// so like if there is SDL_KEYDOWN event you just call
// key_list[event.key.keysym.sym](event)
// and it executes the function you set earlier
// the rest will just be NULL
//
// mouse buttons prob still have to be a switch or maybe not but will need
// separate array prob
// not the most memory firendly solution but pretty elegant
//
// finished it for keyboard
// rn idk if it is a viable option,
// maybe ot will be better to have a sepparate file with this giant 
// switch? we will see
void event_loop(game_t* game){
    SDL_Event event;
    while (SDL_PollEvent(&event)){
        switch (event.type){
            case SDL_QUIT:
                quit(game);
                break;
            case SDL_MOUSEMOTION:
                game->mouse_x = event.motion.x;
                game->mouse_y = event.motion.y;
                break;
            case SDL_MOUSEBUTTONUP:
                switch (event.button.button){
                    case SDL_BUTTON_LEFT:
                        
                        game->right_up = true;
                        break;
                    case SDL_BUTTON_RIGHT:
                        break;
                    default:
                        break;
                }
                break;
            case SDL_MOUSEBUTTONDOWN:
                switch (event.button.button){
                    case SDL_BUTTON_LEFT:
                        
                        game->right_up = false;
                        //game->left_click_action(game); 
                        break;
                    case SDL_BUTTON_RIGHT:
                        break;
                    default:
                        break;
                }
                break;
            case SDL_KEYDOWN:
                run_key(event, game, event.key.keysym.sym);
                break;
            default:
                break;
        } 
    }
}


RETURN_t game_loop(game_t* game){
    event_loop(game);  
    if (!game->right_up) delete_stick(game);
    background_color(game->renderer, GRAY);
    if (game->run_physics) run_ragdoll(&game->engine.physics, &game->ragdoll); 
    render_ragdoll(game->renderer, &game->ragdoll); 
     
    sprintf(game->fps_str, "%.0f", 1/game->engine.physics.delta_time); 
    SDL_Texture* text = text_texture(game->renderer,
            game->fps_str, game->font_40, BLACK); 
    blit_texture(game->renderer, text, 10, 10);
    update(&game->engine);
    return OK;    
}
