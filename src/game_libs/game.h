#ifndef GAME_H
#define GAME_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include "../engine/util.h"
#include "../engine/engine.h"

typedef struct game_t{
    RETURN_t status;
    bool run;

    engine_t engine;
    SDL_Window* window;    
    SDL_Renderer* renderer;
    int window_W;
    int window_H;
    int mouse_x;
    int mouse_y;
    
    void (*keyboard_actions[255])(SDL_Event event, struct game_t*);  
    void (*left_click_action)(struct game_t*);
    
    bool right_up; 

    SDL_Texture* textures[256];
    int textures_len;
    
    ragdoll_object_t ragdoll;
    bool input_point_locked;
    bool run_physics;
    
    TTF_Font* font_40;
    char fps_str[20];
} game_t;

/*
 * Initialise game, basic struct that is passed to game_loop
 * create all needed objects and such
 * return: filled game_t struct or NULL on error
 */
game_t game_init(engine_t engine);

/*
 * Dealocates all content from game_t struct
 * return: Nothing, void function
 */
void clear_game(game_t* game);

/*
 * Game loop, called form main.c in while loop, contains all logic
 * return: value from enum RETURN, OK if frame was created correctly
 * otherwise other error
 */
RETURN_t game_loop(game_t* game);

/*
 * Runs the events
 *
 */
void event_loop(game_t* game);

/*
 * sets game.run to false
 *
 */
void quit();

void create_point(game_t* game);
void toggle_simulation(SDL_Event event, game_t* game);

// deletes stick under the cursor (a circle around cursor)
void delete_stick(game_t* game);
void toggle_locked(SDL_Event event, game_t* game);

void map_key(game_t* game, char key, 
        void (*action)(SDL_Event event, game_t* game));

#endif
