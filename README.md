# Cloth SImulation in C
This repo contains a basic Verlet Integration engine and a cloth simulation demo.
Built in SDL2 with C99 standard.

Not much in this repository is finished but the demo works reasonably well. I probably wont
take up this repo any time soon so dont expect any updates.

This code is licensed under **GPL3** license. Probably will change this to MIT.

#Requirements
 - <a href="https://www.libsdl.org/download-2.0.php"> SDL2 library </a> (it is not included)
 - cmake
 - gcc

# Building
```
mkdir build && cd build
cmake ../
make
./main
```

<br><br>
<img src="https://mpavelka.eu/gitlab_images/cloth.png">
